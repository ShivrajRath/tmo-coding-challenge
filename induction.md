## Developer Induction

### Learning
1. Angular 7+
2. Angular material
3. rxjs
4. @ngrx (store, effects, entities, schematics, store-devtools)
5. hapi.js

### Running on localhost
If the calls are being made directly from browser to iexcloud api, you need a CORS modifier browser extension (e.x: Moesif Orign & CORS Changer)
