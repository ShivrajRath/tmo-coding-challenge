## Task 1
--------------------

### What is done well?
1. Putting the state, actions, effects, reducers under the component folder under libs. This makes easy to relate between these in context of the component.
2. Splitting the app into granular level of actions:
   1. Selection of symbol
   2. Fetching of query
   3. Query fetch success (handled through @ngrx/effect)
   4. Query fetch failure (handled through @ngrx/effect)
3. Use of actions as class for @ngrx/store actions (file price-query.actions.ts). This helps in maintaining immutability

### What would you change?
1. Use @ngrx/schematics instead of @nrwl/schematics since the former keeps getting updated with features
2. Lack of code comments in the verbose code of @ngrx and rxjs could be a time killer for future reference
3. Why keep tsconfig.json keeps repeating?

### Are there any code smells or problematic implementations?
1. The API endpoint isn't right, it works on `https://iexcloud.io` (Fixed)
2. Doesn't run on localhost, needs a documentation for developer to run it in local (Fixed - Added induction.md)
3. The chart component doesn't load (Fixed)
4. Timeperiod in stock component needs to be in stock constant (Fixed)
5. Material should be in a separate module rather than stock module as it may bring in lot of other dependencies
6. API call on the effect should be on a separate service. Since the calls could increase with requirement, it can be made into a separate concern. 