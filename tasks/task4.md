## Task4

1. Implements hapijs API such as 
`http://localhost:3333/stock?symbol=APPL&fromDate=1555916400000&toDate=1556175600000`
to fetch stock on a range of date. This is to support task3 where the fetch has to be made between date range

2. Implements `async` handler for hapi to wait for the api fetch.

3. In memory cache for a symbol and the date range implemented. This can be easily ported to `redis` or `memcache`. Also adds an unique `eTag` for browsers to cache the response

4. Flow : `request > queryHandler > stockHandler > cacheHandler`

5. Implements basic error handling

