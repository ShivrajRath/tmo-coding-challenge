import {
  ResponseToolkit,
  Request
} from 'hapi';
import {
  Query
} from './interface';
import {
  CachingHandler as cachingHandler
} from './caching-handler';
import {
  StockHandler
} from './stock-handler';

export enum HTTP_CODES {
  'NOT_FOUND' = 404,
  'ISE' = 500
}

export class QueryHandler {
  static async fetchStock(request: Request, h: ResponseToolkit) {
    const query: Query = ( < any > request.query);
    if (!query.symbol || !query.fromDate || !query.toDate) {
      return h.response('Incorrect request').code(HTTP_CODES.ISE);
    } else {
      const etag: string = cachingHandler.etag(query);
      const cache = cachingHandler.getCache(etag);
      if (cache) {
        return h.response(cache).etag(etag);
      } else {
        try {
          const response = await new StockHandler(query).fetch();
          cachingHandler.setCache(etag, response);
          return h.response(response).etag(etag);
        } catch (err) {
          return h.response(err).code(HTTP_CODES.ISE);
        }
      }
    }
  }

  static notfound(h: ResponseToolkit) {
    return h.response('404 Error! Page Not Found!').code(HTTP_CODES.NOT_FOUND);
  }
}
