import {
  Query,
  PriceQueryResponse
} from './interface';

export class CachingHandler {

  // simple in memory cache
  private static _cache: any = {};

  // gets an unique string to cache the request on the browser
  static etag(object: Query) {
    return Buffer.from(Object.values(object).join('')).toString('base64');
  }

  static getCache(key: string): PriceQueryResponse[] {
    return this._cache[key];
  }

  static setCache(key: string, priceQuery: PriceQueryResponse[]) {
    this._cache[key] = priceQuery;
  }
}
