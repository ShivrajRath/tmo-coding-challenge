import {
  Query,
  PriceQueryResponse
} from "./interface";
import {
  CachingHandler as cachingHandler
} from './caching-handler';
import {
  environment
} from '../environments/environment';

const request = require('request');

export class StockHandler {
  constructor(private query: Query) {}

  private async maxFetch(): Promise < PriceQueryResponse[] > {
    return new Promise((resolve, reject) => {
      const cache = cachingHandler.getCache(`max${this.query.symbol}`);
      if (cache) {
        resolve(cache);
      } else {
        request(`https://iexcloud.io/beta/stock/${this.query.symbol}/chart/1m?token=${environment.token}`,
          (error, response, body) => {
            if (error) {
              reject(error);
            } else {
              try {
                body = JSON.parse(body);
                cachingHandler.setCache(`max${this.query.symbol}`, body);
                resolve(body);
              } catch (err) {
                reject(body);
              }
            }
          })
      }
    })
  }

  private filter(maxResult: PriceQueryResponse[]): PriceQueryResponse[] {
    const fromDate = parseInt(this.query.fromDate, 10),
        toDate = parseInt(this.query.toDate, 10);
    const filterData: PriceQueryResponse[] = maxResult.filter((stock: PriceQueryResponse) => {
      const stockDate = new Date(stock.date).getTime();
      return stockDate >= fromDate && stockDate <= toDate;
    });
    return filterData;
  }

  public async fetch(): Promise < PriceQueryResponse[] > {
    const maxResult = await this.maxFetch();
    return new Promise < PriceQueryResponse[] > ((resolve, reject) => {
      try {
        resolve(this.filter(maxResult));
      } catch (err) {
        reject('unable to fetch the data');
      }
      return resolve;
    });
  }
}
