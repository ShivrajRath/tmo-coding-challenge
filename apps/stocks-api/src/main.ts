/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import {
  Server
} from 'hapi';
import { QueryHandler as queryHandler } from './app/query-handler';

const init = async () => {
  const server = new Server({
    port: 3333,
    host: 'localhost'
  });

  server.route({
    method: 'GET',
    path: '/stock',
    handler: async function(request, reply) {
      const response = await queryHandler.fetchStock(request, reply);
      return response;
    }
  });

  server.route({
    method: '*',
    path: '/{any*}',
    handler: function (request, reply) {
      return queryHandler.notfound(reply);
    }
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();