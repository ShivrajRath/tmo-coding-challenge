import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import * as stockConstants from './stocks.constants';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  stockPickerForm: FormGroup;
  symbol: string;
  // period: string;
  maxDate = new Date();
  minToDate = new Date();
  fromDate: Date;
  toDate: Date;

  quotes$ = this.priceQuery.priceQueries$;

  // timePeriods = stockConstants.TIME_PERIODS;

  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      // period: [null, Validators.required],
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.stockPickerForm.
      valueChanges.subscribe(this.fetchQuote.bind(this));
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, fromDate, toDate } = this.stockPickerForm.value;
      this.priceQuery.fetchQuote(symbol, fromDate.getTime(), toDate.getTime());
    }
  }

  fromDateChanged($event: MatDatepickerInputEvent<Date>) {
    this.minToDate = this.fromDate = $event.value;
  }
}
